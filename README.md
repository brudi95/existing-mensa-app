# MensaApp

Bei diesem Projekt handelt es sich um eine MensaApp,
welche dem Nutzer die Möglichkeit bietet, Mahlzeiten einzusehen.
Desweiteren besteht die Möglichkeit weitere Standorte in die Suche mit einzubeziehen.

## Getting Started

Grundlegend für den Betrieb der Applikation sind die passende Entwicklungsumgebung,
sowie ein JDK.

### Prerequisites

Vorraussetzungen:

```
Android Studio
```
```
Gradle
```
```
Java
```

### Installing

Nach der Installation muss das bestehende Projekt in Android Studio importiert werden.
Nach dem der Build Prozess durchlaufen ist, kann das Projekt über einen Emulator
oder ein Android Device gestartet werden.

## Deployment

Diese Applikation behandelt keine Differenzierung zwischen Test und Live Umgebung.

## Built With

* [Android Studio](https://developer.android.com/studio/index.html) - Entwicklungsumgebung
* [Gradle](https://gradle.org/) - Abhängigkeiten verwalten
* [JAVA](http://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html) - Programmiersprache

## Contributing

Eine Liste der Unterstützer ist nicht vorhanden.

## Authors

* **Stephan Nordheim** - *Initial work* - [stenod](https://bitbucket.org/stenod/)

## License

Das Projekt unterliegt der MIT License.

## Acknowledgments

* Es liegen keine weiteren besonderen Anerkennungen vor
